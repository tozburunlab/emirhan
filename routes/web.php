<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::post('login',[\App\Http\Controllers\LoginController::class,'login'])->name('login.post');
Route::get('login',[\App\Http\Controllers\LoginController::class,'index'])->name('login');


Route::middleware([\App\Http\Middleware\Authenticate::class])->group(function (){
Route::get('/',[\App\Http\Controllers\ProductController::class,'index']);
Route::get('profile/{id}',[\App\Http\Controllers\UserController::class,'profile'])->name('user.profile');
Route::PUT('profile-update',[\App\Http\Controllers\UserController::class,'profileUpdate'])->name('user.profile-update');
Route::get('logout',[\App\Http\Controllers\UserController::class,'logout'])->name('user.logout');
Route::delete('product-file-delete/{id}',[\App\Http\Controllers\ProductController::class,'deleteFile'])->name('product.file-delete');
Route::resource('product',\App\Http\Controllers\ProductController::class);
Route::resource('user',\App\Http\Controllers\UserController::class);
Route::resource('use-area',\App\Http\Controllers\UseAreaController::class);
Route::resource('use-area-type',\App\Http\Controllers\UseAreaTypeController::class);
Route::resource('use-product',\App\Http\Controllers\UseProductController::class);
Route::resource('manufacturer',\App\Http\Controllers\ManufacturerControler::class);
Route::resource('tag',\App\Http\Controllers\TagController::class);
});
