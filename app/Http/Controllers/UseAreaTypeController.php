<?php

namespace App\Http\Controllers;

use App\Models\UseAreaType;
use Illuminate\Http\Request;

class UseAreaTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $useAreaTypes = UseAreaType::all();
        return  view('UseAreaType.index',compact('useAreaTypes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('UseAreaType.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = UseAreaType::create($request->all());
        $status = $data ? 200 : 500;
        return  redirect(route('use-area-type.index'))->with('status',$status);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = UseAreaType::find($id);
        return view('UseAreaType.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = UseAreaType::find($id)->update($request->all());
        $status = $data ? 200 : 500;
        return redirect(route('use-area-type.edit',$id))->with('status',$status);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = UseAreaType::find($id)->delete();
        $status = $data ? 200 : 500;
        return  redirect(route('use-area-type.index'))->with('status',$status);
    }
}
