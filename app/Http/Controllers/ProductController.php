<?php

namespace App\Http\Controllers;

use App\Models\Manufacturer;
use App\Models\Product;
use App\Models\ProductFile;
use App\Models\Tag;
use App\Models\UseArea;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ProductController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $products = Product::query();

        if($request->filled('use_area_id')){
            $products = $products->whereHas('pivot',function ($query) use($request){
               return $query->where('use_area_id',$request->use_area_id);
            });
        }

        if ($request->filled('manufacturer_id')){
            $products = $products->where('manufacturer_id',$request->manufacturer_id);
        }

        if ($request->filled('name')){
            $products = $products->where('name','like','%'.$request->name.'%');
        }


        if ($request->filled('tag_ids')){
            foreach ($request->tag_ids as $tag){
                $products = $products->whereHas('tag',function ($query) use($tag){
                    return $query->where('tag_id',$tag);
                });
            }
        }

        $products = $products->orderByDesc('id')->paginate(30);


        $useAreas = UseArea::all();
        $manufacturers = Manufacturer::all();
        $tags = Tag::all();
        return view('Product.index',compact('products','useAreas','manufacturers','tags'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $manufacturers = Manufacturer::all();
        $tags = Tag::all();
        return view('Product.create',compact('manufacturers','tags'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $image = null;

        if ($request->hasFile('image')) {

            $uploadedFile = $request->file('image');

            $image = time() . $uploadedFile->getClientOriginalName();
            Storage::disk('product_images')->putFileAs(
                '',
                $uploadedFile,
                $image
            );
        }

        $data = Product::create($request->except(['image', 'tags']));
        $data->image = $image;
        $data->save();

        $status = $data ? 200 : 500;

        if ($request->filled('tags')){
        foreach ($request->tags as $tag) {
            $data->tag()->attach($tag, [
                'created_at' => date('Y-m-d H:i:s')
            ]);
        }
        }

        return redirect(route('product.index'))->with('status', $status);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Product::find($id);
        $data->remaining = $data->quantity - abs($data->remainingQuantity());
        $manufacturers = Manufacturer::all();
        $tags = Tag::all();
        $productTags = $data->tag->pluck('id');
        $productTags = $productTags->toArray();
        return view('Product.edit',compact('data','manufacturers','tags','productTags'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $product = Product::find($id);
        if ($request->hasFile('file')){
            $request->validate([
                'file_name' => 'required'
            ]);
            $uploadedFile = $request->file('file');

            $filename = time().$uploadedFile->getClientOriginalName();
            Storage::disk('public_uploads')->putFileAs(
               '',
                $uploadedFile,
                $filename
            );

            ProductFile::create([
                'name' => $request->file_name,
                'product_id' => $product->id,
                'path' => '/files/'.$filename
            ]);
        }

        $image = $request->old_image;
        if ($request->hasFile('image')){

            $uploadedFile = $request->file('image');

            $image = time().$uploadedFile->getClientOriginalName();
            Storage::disk('product_images')->putFileAs(
                '',
                $uploadedFile,
                $image
            );
            Storage::disk('product_images')->delete($request->old_image);
        }

        $product->tag()->detach();
        foreach ($request->tags as $tag){
            $product->tag()->attach($tag,[
                'created_at' => date('Y-m-d H:i:s')
            ]);
        }

       $status = $product->update([
            'name' => $request->name,
            'part_code' => $request->part_code,
            'web_site' => $request->web_site,
            'quantity' => $request->quantity,
            'note' => $request->note,
            'image' => $image,
            'manufacturer_id' => $request->manufacturer_id
        ]);

        $status = $product ? 200 : 500;
        return  redirect(route('product.edit',$id))->with('status',$status);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Product::find($id)->delete();
        $status = $data ? 200 : 500;
        return  redirect(route('product.index'))->with('status',$status);
    }

    public function deleteFile($id){
        $file = ProductFile::find($id);
        Storage::disk('public_uploads')->delete('/files/'.$file->path);
        $file->delete();
        return back()->with('status','200');
    }
}
