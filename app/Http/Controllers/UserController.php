<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user()->delete_permission != 1){
            return redirect(route('product.index'));
        }
        $users = User::all();
        return  view('User.index',compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(Auth::user()->delete_permission != 1){
            return redirect(route('product.index'));
        }
        return view('User.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(Auth::user()->delete_permission != 1){
            return redirect(route('product.index'));
        }
        $request->validate([
            'password' => 'required|confirmed',
            'email' => 'required|unique:users|',
        ]);
        $request->request->remove('password_confirmation');

        $data = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'telephone' => $request->telephone,
            'delete_permission' => $request->delete_permission
        ]);
        $status = $data ? 201 : 500;
        return redirect(route('user.index'))->with('status',$status);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(Auth::user()->delete_permission != 1){
            return redirect(route('product.index'));
        }
        $data = User::find($id);
        return  view('User.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(Auth::user()->delete_permission != 1){
            return redirect(route('product.index'));
        }
        $request->validate([
            'email' => 'required|unique:users,email,'.$id,
        ]);
        if ($request->filled('password') || $request->filled('password_confirmation')){
        $request->validate([
            'password' => 'required|confirmed',
        ]);
        }else{
            $request->request->remove('password');
        }

        $request->request->remove('password_confirmation');

        $data = User::find($id);



        $password = $request->filled('password') ? bcrypt($request->password) : $data->password;

        $data->update([
            'name' => $request->name,
            'email' => $request->email,
            'password' => $password,
            'telephone' => $request->telephone,
            'delete_permission' => $request->delete_permission
        ]);
        $status = $data ? 200 : 500;
       return  redirect(route('user.edit',$id))->with('status',$status);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Auth::user()->delete_permission != 1){
            return redirect(route('product.index'));
        }
        $data = User::find($id)->delete();
        $status = $data ? 200 : 500;
        return  redirect(route('user.index'))->with('status',$status);
    }

    public function logout(){
        Session::flush();
        Auth::logout();
        return redirect(route('login'));
    }

    public function profile($id){
        if (Auth::user()->id != $id){
            return redirect(route('product.index'));
        }

        $data = Auth::user();
        return view('user.profile',compact('data'));
    }

    public function profileUpdate(Request $request){
        $request->validate([
            'email' => 'required|unique:users,email,'.Auth::user()->id,
        ]);
        if ($request->filled('password') || $request->filled('password_confirmation')){
            $request->validate([
                'password' => 'required|confirmed',
            ]);
        }else{
            $request->request->remove('password');
        }

        $request->request->remove('password_confirmation');

        $data = User::find(Auth::user()->id);

        $password = $request->filled('password') ? bcrypt($request->password) : $data->password;

        $data->update([
            'name' => $request->name,
            'email' => $request->email,
            'password' => $password,
            'telephone' => $request->telephone,
        ]);
        $status = $data ? 200 : 500;
        return  redirect(route('user.profile',$data->id))->with('status',$status);
    }
}
