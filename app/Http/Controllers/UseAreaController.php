<?php

namespace App\Http\Controllers;

use App\Models\UseArea;
use App\Models\UseAreaType;
use Illuminate\Http\Request;

class UseAreaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $useAreas = UseArea::with('useAreaType')->get();

        return  view('UseArea.index',compact('useAreas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $useAreaTypes = UseAreaType::all();
        return  view('UseArea.create',compact('useAreaTypes'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = UseArea::create($request->all());
        $status = $data ? 200 : 500;
        return  redirect(route('use-area.index'))->with('status',$status);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $useAreaTypes = UseAreaType::all();
        $data = UseArea::find($id);
        return view('UseArea.edit',compact('data','useAreaTypes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = UseArea::find($id)->update($request->all());
        $status = $data ? 200 : 500;
        return  redirect(route('use-area.edit',$id))->with('status',$status);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = UseArea::find($id)->delete();
        $status = $data ? 200 : 500;
        return  redirect(route('use-area.index'))->with('status',$status);
    }
}
