<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\ProductUseArea;
use App\Models\UseArea;
use App\Models\User;
use Illuminate\Http\Request;

class UseProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $useProducts = ProductUseArea::with(['product','useArea'])->paginate(30);
        return  view('UseProduct.index',compact('useProducts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $useAreas = UseArea::all();
        $products = Product::all();
        $users = User::all();
        return view('UseProduct.create',compact('useAreas','products','users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $product = Product::find($request->product_id);
        $product->useArea()->attach($request->use_area_id,[
               'quantity' => $request->quantity,
                'date' => $request->date,
                'action' => $request->action,
                'user_id' => $request->user_id,
                'created_at' => date('Y-m-d H:i:s')
           ]);
        $status = 200;
        return redirect(route('use-product.index'))->with('status',$status);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = ProductUseArea::find($id);
        $useAreas = UseArea::all();
        $products = Product::all();
        $users = User::all();
        return view('UseProduct.edit',compact('data','products','useAreas','users'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = ProductUseArea::find($id)->update([
            'product_id' => $request->product_id,
            'use_area_id' => $request->use_area_id,
            'user_id' => $request->user_id,
            'date' => $request->date,
            'action' => $request->action,
            'quantity' => $request->quantity
        ]);

        $status = $data ? 200 : 500;
        return  redirect(route('use-product.edit',$id))->with('status',$status);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = ProductUseArea::find($id)->delete();
        $status = $data ? 200 : 500;
        return  redirect(route('use-product.index'))->with('status',$status);
    }
}
