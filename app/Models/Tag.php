<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    use HasFactory;
    protected $fillable = ['name'];

    public static function booted()
    {
        static::deleting(function ($useAreaType) {
            $dataCheck = $useAreaType->products()->get();
            if ($dataCheck->isNotEmpty()) {
                dd('It cannot be deleted because there is other data that has a dependency on the data you want to delete.');
            }
        });
    }

    public function products(){
        return $this->belongsToMany(Product::class);
    }
}
