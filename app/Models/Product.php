<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;
    protected $guarded = [];

    public static function booted()
    {
        static::deleting(function ($product) {
            $dataCheck = $product->pivot()->get();
            if ($dataCheck->isNotEmpty()) {
                dd('It cannot be deleted because there is other data that has a dependency on the data you want to delete.');
            }
        });
    }

    public function tag(){
        return $this->belongsToMany(Tag::class);
    }

    public function useArea(){
        return $this->belongsToMany(UseArea::class)->withPivot('user_id','quantity','date','action');
    }

    public function manufacturer(){
        return $this->belongsTo(Manufacturer::class);
    }

    public function remainingQuantity(){
        $useProductSupplied = ProductUseArea::where('product_id',$this->id)->where('action',1)->sum('quantity');
        $useProductReceived = ProductUseArea::where('product_id',$this->id)->where('action',0)->sum('quantity');
        $result = intval($useProductSupplied) - intval($useProductReceived);
        return $result;
    }

    public function pivot(){
        return $this->hasMany(ProductUseArea::class);
    }

    public function files(){
        return $this->hasMany(ProductFile::class);
    }
}
