<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductUseArea extends Model
{
    use HasFactory;
    protected $table = 'product_use_area';
    protected $guarded = [];

    public function product(){
        return $this->belongsTo(Product::class);
    }

    public function useArea(){
        return $this->belongsTo(UseArea::class);
    }
}
