<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UseArea extends Model
{
    use HasFactory;
    protected $guarded = [];


    public function useAreaType(){
        return $this->belongsTo(UseAreaType::class)->withDefault(['name' => '-']);
    }
}
