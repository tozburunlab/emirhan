<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UseAreaType extends Model
{
    use HasFactory;
    protected $guarded  = [];

    public static function booted()
    {
        static::deleting(function ($useAreaType) {
            $dataCheck = $useAreaType->useArea()->get();
            if ($dataCheck->isNotEmpty()) {
            dd('It cannot be deleted because there is other data that has a dependency on the data you want to delete.');
            }
        });
    }

    public function useArea(){
        return $this->belongsTo(UseArea::class,'id','use_area_type_id');
    }
}
