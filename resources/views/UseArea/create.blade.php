@extends('layout')
@section('content')
    <div class="container-fluid">
        <header class="section-header">
            <div class="tbl">
                <div class="tbl-row">
                    <div class="tbl-cell">
                        <h3>Use Area Add</h3>
                    </div>
                </div>
            </div>
        </header>
        <div class="box-typical box-typical-padding">
            <form action="{{route('use-area.store')}}" method="POST">
                @csrf
                <div class="row">
                    <div class="col-md-6">
                        <fieldset class="form-group">
                            <label class="form-label semibold">Name</label>
                            <input type="text" class="form-control" name="name" value="" required maxlength="255">
                        </fieldset>
                    </div>
                    <div class="col-md-6">
                        <fieldset class="form-group">
                            <label class="form-label semibold">Use Area Type</label>
                            <select name="use_area_type_id" class="form-control" required>
                                <option value=""></option>
                            @foreach($useAreaTypes as $value)
                                <option value="{{$value->id}}">{{$value->name}}</option>
                            @endforeach
                            </select>
                        </fieldset>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <label class="form-label semibold">Note</label>
                        <textarea class="form-control" name="note"></textarea>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-6 float-right">
                            <button type="submit" class="btn btn-success float-right"><i class="fa fa-save"></i> Save</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
