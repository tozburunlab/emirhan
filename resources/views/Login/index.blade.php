<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Login</title>

    <link href="/assets/img/favicon.144x144.png" rel="apple-touch-icon" type="image/png" sizes="144x144">
    <link href="/assets/img/favicon.114x114.png" rel="apple-touch-icon" type="image/png" sizes="114x114">
    <link href="/assets/img/favicon.72x72.png" rel="apple-touch-icon" type="image/png" sizes="72x72">
    <link href="/assets/img/favicon.57x57.png" rel="apple-touch-icon" type="image/png">
    <link href="/assets/img/favicon.png" rel="icon" type="image/png">
    <link href="/assets/img/favicon.ico" rel="shortcut icon">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link rel="stylesheet" href="/assets/css/separate/pages/login.min.css">
    <link rel="stylesheet" href="/assets/css/lib/font-awesome/font-awesome.min.css">
    <link rel="stylesheet" href="/assets/css/lib/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="/assets/css/main.css">
</head>
<body>

<div class="page-center">
    <div class="page-center-in">
        <div class="container-fluid">
            <form action="{{route('login.post')}}" method="POST" class="sign-box">
                @csrf
                <div class="sign-avatar">
                    <img src="/assets/img/avatar-sign.png" alt="">
                </div>
                <header class="sign-title">Sign In</header>
                <div class="form-group">
                    <input type="email" name="email" class="form-control" placeholder="E-Mail" required />
                </div>
                <div class="form-group">
                    <input type="password" name="password" class="form-control" placeholder="Password" required />
                </div>
                <button type="submit" class="btn btn-rounded">Sign in</button>
            </form>
        </div>
    </div>
</div><!--.page-center-->


<script src="/assets/js/lib/jquery/jquery-3.2.1.min.js"></script>
<script src="/assets/js/lib/popper/popper.min.js"></script>
<script src="/assets/js/lib/tether/tether.min.js"></script>
<script src="/assets/js/lib/bootstrap/bootstrap.min.js"></script>
<script src="/assets/js/plugins.js"></script>
<script type="text/javascript" src="/assets/js/lib/match-height/jquery.matchHeight.min.js"></script>
<script>
    $(function() {
        $('.page-center').matchHeight({
            target: $('html')
        });

        $(window).resize(function(){
            setTimeout(function(){
                $('.page-center').matchHeight({ remove: true });
                $('.page-center').matchHeight({
                    target: $('html')
                });
            },100);
        });
    });
</script>
<script src="/assets/js/app.js"></script>
</body>
</html>
@if(session()->has('error'))
    <script type="text/javascript">
        alert('Email address or password is incorrect');
    </script>
@endif
