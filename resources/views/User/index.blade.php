@extends('layout')
@section('content')
    <div class="container-fluid">
        <header class="section-header">
            <div class="tbl">
                <div class="tbl-row">
                    <div class="tbl-cell">
                        <h3>Users</h3>
                    </div>
                    <div class="float-right">
                        <a href="{{route('user.create')}}"> <button type="button" class="btn btn-warning"><i class="fa fa-plus"></i> User Add</button></a>
                    </div>
                </div>
            </div>
        </header>
        <div class="box-typical box-typical-padding">
            <table id="table-sm" class="table table-bordered table-hover table-sm">
                <thead>
                <tr>
                    <th class="text-center" width="1">#</th>
                    <th class="text-center">Created At</th>
                    <th class="text-center">Fullname</th>
                    <th class="text-center">E-Mail</th>
                    <th class="text-center">Telephone</th>
                    <th class="text-center">Admin</th>
                    <th class="text-center">Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($users as $value)
                <tr>
                    <td class="text-center">{{$value->id}}</td>
                    <td class="text-center">{{$value->created ? date('d.m.Y',strtotime($value->created_at)) : '-'}}</td>
                    <td class="text-center">{{$value->name}}</td>
                    <td class="text-center">{{$value->email}}</td>
                    <td class="text-center">{{$value->telephone}}</td>
                    <td class="text-center">{{$value->delete_permission ? 'Yes' : 'No'}}</td>
                    <td class="text-center">
                        <div class="">
                            <a href="{{route('user.edit',$value->id)}}"><button type="button" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i> Edit</button></a>
                            @if(\Illuminate\Support\Facades\Auth::user()->delete_permission)
                            <form action="{{route('user.destroy',$value->id)}}" method="POST">@csrf @method('delete')<button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Are you sure?')"><i class="fa fa-trash-o"></i> Delete</button></form>
                                @endif
                        </div>
                    </td>
                </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
