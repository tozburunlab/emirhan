@extends('layout')
@section('content')
    <div class="container-fluid">
        <header class="section-header">
            <div class="tbl">
                <div class="tbl-row">
                    <div class="tbl-cell">
                        <h3>User Edit</h3>
                    </div>
                </div>
            </div>
        </header>
        <div class="box-typical box-typical-padding">
            <form action="{{route('user.update',$data->id)}}" method="POST">
                @csrf
                @method('PUT')
                <div class="row">
                    <div class="col-md-6">
                        <fieldset class="form-group">
                            <label class="form-label semibold">Fullname</label>
                            <input type="text" class="form-control" name="name" value="{{$data->name}}" required maxlength="255">
                        </fieldset>
                    </div>
                    <div class="col-md-6">
                        <fieldset class="form-group">
                            <label class="form-label semibold">Telephone</label>
                            <input type="text" class="form-control" name="telephone" value="{{$data->telephone}}" maxlength="12">
                        </fieldset>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <fieldset class="form-group">
                            <label class="form-label semibold">Password</label>
                            <input type="password" class="form-control" name="password" value="" maxlength="20">
                        </fieldset>
                    </div>
                    <div class="col-md-6">
                        <fieldset class="form-group">
                            <label class="form-label semibold">Password Confirm</label>
                            <input type="password" class="form-control" name="password_confirmation" value="" maxlength="20">
                        </fieldset>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <fieldset class="form-group">
                            <label class="form-label semibold">Admin</label>
                            <select name="delete_permission" class="form-control" required>
                                <option value="0" {{$data->delete_permission == 0 ? 'selected' : ''}}>No</option>
                                <option value="1" {{$data->delete_permission == 1 ? 'selected' : ''}}>Yes</option>
                            </select>
                        </fieldset>
                    </div>

                    <div class="col-md-6">
                        <fieldset class="form-group">
                            <label class="form-label semibold">Email</label>
                            <input type="email" class="form-control" name="email" value="{{$data->email}}" required maxlength="255">
                        </fieldset>
                    </div>

                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-6 float-right">
                            <button type="submit" class="btn btn-success float-right"><i class="fa fa-save"></i> Save</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
