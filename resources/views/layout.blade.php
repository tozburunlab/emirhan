<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Panel</title>
    <link href="/assets/img/favicon.144x144.png" rel="apple-touch-icon" type="image/png" sizes="144x144">
    <link href="/assets/img/favicon.114x114.png" rel="apple-touch-icon" type="image/png" sizes="114x114">
    <link href="/assets/img/favicon.72x72.png" rel="apple-touch-icon" type="image/png" sizes="72x72">
    <link href="/assets/img/favicon.57x57.png" rel="apple-touch-icon" type="image/png">
    <link href="/assets/img/favicon.png" rel="icon" type="image/png">
    <link href="/assets/img/favicon.ico" rel="shortcut icon">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link rel="stylesheet" href="/assets/css/separate/vendor/tags_editor.min.css">
    <link rel="stylesheet" href="/assets/css/separate/vendor/bootstrap-select/bootstrap-select.min.css">
    <link rel="stylesheet" href="/assets/css/separate/vendor/select2.min.css">
    <link rel="stylesheet" href="/assets/css/lib/font-awesome/font-awesome.min.css">
    <link rel="stylesheet" href="/assets/css/separate/vendor/bootstrap-touchspin.min.css">
    <link rel="stylesheet" href="/assets/css/lib/font-awesome/font-awesome.min.css">
    <link rel="stylesheet" href="/assets/css/lib/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="/assets/css/main.css">
    @yield('head')
</head>
<body class="with-side-menu">
<header class="site-header">
    <div class="container-fluid">
        <button id="show-hide-sidebar-toggle" class="show-hide-sidebar">
            <span>toggle menu</span>
        </button>
        <button class="hamburger hamburger--htla">
            <span>toggle menu</span>
        </button>
        <div class="site-header-content">
            <div class="site-header-content-in">
                <div class="site-header-shown">
                    <div class="dropdown user-menu">
                        <button class="dropdown-toggle" id="dd-user-menu" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            {{\Illuminate\Support\Facades\Auth::user()->name}}

                            <img src="/assets/img/avatar-2-64.png" alt="">
                        </button>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dd-user-menu">

                            <a class="dropdown-item" href="{{route('user.profile',\Illuminate\Support\Facades\Auth::user()->id)}}"><span class="font-icon glyphicon glyphicon-user"></span>Profile</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="{{route('user.logout')}}"><span class="font-icon glyphicon glyphicon-log-out"></span>Logout</a>
                        </div>
                    </div>

                    <button type="button" class="burger-right">
                        <i class="font-icon-menu-addl"></i>
                    </button>
                </div><!--.site-header-shown-->

                <div class="mobile-menu-right-overlay"></div>
            </div><!--site-header-content-in-->
        </div><!--.site-header-content-->
    </div><!--.container-fluid-->
</header><!--.site-header-->
<div class="mobile-menu-left-overlay"></div>
<nav class="side-menu">
    <ul class="side-menu-list">
        @if(\Illuminate\Support\Facades\Auth::user()->delete_permission)
        <li class="blue-dirty">
            <a href="{{route('user.index')}}">
                <i class="font-icon font-icon-notebook"></i>
                <span class="lbl">Users</span>
            </a>
        </li>
        @endif

        <li class="blue-dirty">
            <a href="{{route('use-area-type.index')}}">
                <i class="font-icon font-icon-notebook"></i>
                <span class="lbl">Use Area Types</span>
            </a>
        </li>

        <li class="blue-dirty">
            <a href="{{route('use-area.index')}}">
                <i class="font-icon font-icon-notebook"></i>
                <span class="lbl">Use Areas</span>
            </a>
        </li>

            <li class="blue-dirty">
                <a href="{{route('tag.index')}}">
                    <i class="font-icon font-icon-notebook"></i>
                    <span class="lbl">Tags</span>
                </a>
            </li>

        <li class="blue-dirty">
                <a href="{{route('manufacturer.index')}}">
                    <i class="font-icon font-icon-notebook"></i>
                    <span class="lbl">Manufacturer</span>
                </a>
        </li>

        <li class="blue-dirty">
            <a href="{{route('product.index')}}">
                <i class="font-icon font-icon-notebook"></i>
                <span class="lbl">Products</span>
            </a>
        </li>

        <li class="blue-dirty">
            <a href="{{route('use-product.index')}}">
                <i class="font-icon font-icon-notebook"></i>
                <span class="lbl">Use Product</span>
            </a>
        </li>
    </ul>
</nav><!--.side-menu-->
<div class="page-content">
    <div class="container">
        @if($errors->any())
            {!!  implode('', $errors->all('<div class="alert alert-danger alert-fill alert-close alert-dismissible fade show" role="alert">  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button> :message</div>')) !!}
        @endif
    </div>
    @yield('content')
</div><!--.page-content-->
<script src="/assets/js/lib/jquery/jquery-3.2.1.min.js"></script>
<script src="/assets/js/lib/popper/popper.min.js"></script>
<script src="/assets/js/lib/tether/tether.min.js"></script>
<script src="/assets/js/lib/bootstrap/bootstrap.min.js"></script>
<script src="/assets/js/plugins.js"></script>
<script src="/assets/js/lib/bootstrap-touchspin/jquery.bootstrap-touchspin.min.js"></script>
<script src="/assets/js/app.js"></script>
<script src="/assets/js/lib/bootstrap-notify/bootstrap-notify.min.js"></script>
<script src="/assets/js/lib/jquery-tag-editor/jquery.caret.min.js"></script>
<script src="/assets/js/lib/jquery-tag-editor/jquery.tag-editor.min.js"></script>
<script src="/assets/js/lib/bootstrap-select/bootstrap-select.min.js"></script>
<script src="/assets/js/lib/select2/select2.full.min.js"></script>

@if(session()->has('status') && session('status') == 200 || session('status') == 201)
<script type="text/javascript">
    $.notify({
        icon: 'font-icon font-icon-check-circle',
        title: '<strong>Successful</strong>',
        message: ''
    },{
        type: 'success'
    });
</script>
@elseif(session()->has('status') && session('status') == 400 || session('status') == 500)
    <script type="text/javascript">
        $.notify({
            icon: 'font-icon font-icon-check-circle',
            title: '<strong>Error</strong>',
            message: ''
        },{
            type: 'error'
        });
    </script>

    {{dd($errors)}}
@endif
@yield('footer')
</body>
</html>
