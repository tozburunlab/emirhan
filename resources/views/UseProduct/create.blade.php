@extends('layout')
@section('content')
    <div class="container-fluid">
        <header class="section-header">
            <div class="tbl">
                <div class="tbl-row">
                    <div class="tbl-cell">
                        <h3>Use Product</h3>
                    </div>
                </div>
            </div>
        </header>
        <div class="box-typical box-typical-padding">
            <form action="{{route('use-product.store')}}" method="POST">
                @csrf
                <div class="row">
                    <div class="col-md-6">
                        <fieldset class="form-group">
                            <label class="form-label semibold">Product</label>
                            <select name="product_id" class="form-control" required>
                                <option value=""></option>
                                @foreach($products as $product)
                                    <option value="{{$product->id}}">{{$product->name}}</option>
                                @endforeach
                            </select>
                        </fieldset>
                    </div>
                    <div class="col-md-6">
                        <fieldset class="form-group">
                            <label class="form-label semibold">Use Area</label>
                            <select name="use_area_id" class="form-control" required>
                                <option value=""></option>
                                @foreach($useAreas as $useArea)
                                    <option value="{{$useArea->id}}">{{$useArea->name}}</option>
                                @endforeach
                            </select>
                        </fieldset>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <fieldset class="form-group">
                            <label class="form-label semibold">Quantity</label>
                            <input type="number" class="form-control" name="quantity" value="" required>
                        </fieldset>
                    </div>

                    <div class="col-md-6">
                        <fieldset class="form-group">
                            <label class="form-label semibold">User</label>
                            <select name="user_id" class="form-control" required>
                                <option value=""></option>
                                @foreach($users as $user)
                                    <option value="{{$user->id}}">{{$user->name}}</option>
                                @endforeach
                            </select>
                        </fieldset>
                    </div>

                </div>

                <div class="row">
                    <div class="col-md-6">
                        <fieldset class="form-group">
                            <label class="form-label semibold">Date</label>
                            <input type="date" class="form-control" name="date" value="{{date('Y-m-d')}}" required>
                        </fieldset>
                    </div>

                    <div class="col-md-6">
                        <fieldset class="form-group">
                            <label class="form-label semibold">Process</label>
                            <select name="action" class="form-control" required>
                                <option value=""></option>
                                <option value="1">Supplied product</option>
                                <option value="0">Received product</option>
                            </select>
                        </fieldset>
                    </div>


                </div>


                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-6 float-right">
                            <button type="submit" class="btn btn-success float-right"><i class="fa fa-save"></i> Save</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
