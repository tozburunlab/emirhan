@extends('layout')
@section('content')
    <div class="container-fluid">
        <header class="section-header">
            <div class="tbl">
                <div class="tbl-row">
                    <div class="tbl-cell">
                        <h3>Use Product</h3>
                    </div>
                    <div class="float-right">
                        <a href="{{route('use-product.create')}}"> <button type="button" class="btn btn-warning"><i class="fa fa-plus"></i> Use Product</button></a>
                    </div>
                </div>
            </div>
        </header>
        <div class="box-typical box-typical-padding">

            <table id="table-sm" class="table table-bordered table-hover table-sm">
                <thead>
                <tr>
                    <th class="text-center" width="1">#</th>
                    <th class="text-center">Name</th>
                    <th class="text-center">Use Area Name</th>
                    <th class="text-center">Process</th>
                    <th class="text-center">Quantity</th>
                    <th class="text-center">Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($useProducts as $value)
                    <tr>
                        <td class="text-center">{{$value->id}}</td>
                        <td class="text-center">{{$value->product->name}}</td>
                        <td class="text-center">{{$value->useArea->name}}</td>
                        <td class="text-center">{{$value->action ? 'Supplied product' : 'Received product'}}</td>
                        <td class="text-center">{{$value->quantity}}</td>
                        <td class="text-center">
                            <div class="">
                                <a href="{{route('use-product.edit',$value->id)}}"><button type="button" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i> Edit</button></a>
                                @if(\Illuminate\Support\Facades\Auth::user()->delete_permission)
                                <form action="{{route('use-product.destroy',$value->id)}}" method="POST">@csrf @method('delete')<button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Are you sure?')"><i class="fa fa-trash-o"></i> Delete</button></form>
                                @endif
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>


            <div class="d-flex justify-content-center">
                {!! $useProducts->links() !!}
            </div>

        </div>


    </div>
@endsection
