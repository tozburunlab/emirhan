@extends('layout')
@section('content')
    <div class="container-fluid">
        <header class="section-header">
            <div class="tbl">
                <div class="tbl-row">
                    <div class="tbl-cell">
                        <h3>Product Edit</h3>
                        <div class="float-right">
                            <img src="/product_images/{{$data->image}}" width="159">
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <div class="box-typical box-typical-padding">
            <form action="{{route('product.update',$data->id)}}" method="POST" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <input type="hidden" name="old_image" value="{{$data->image}}">
                <div class="row">
                    <div class="col-md-6">
                        <fieldset class="form-group">
                            <label class="form-label semibold">Name</label>
                            <input type="text" class="form-control" name="name" value="{{$data->name}}" required maxlength="500">
                        </fieldset>
                    </div>
                    <div class="col-md-6">
                        <fieldset class="form-group">
                            <label class="form-label semibold">Part Code</label>
                            <input type="text" class="form-control" name="part_code" value="{{$data->part_code}}" maxlength="300">
                        </fieldset>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <fieldset class="form-group">
                            <label class="form-label semibold">Web Site</label>
                            <input type="text" class="form-control" name="web_site" value="{{$data->web_site}}" maxlength="255">
                        </fieldset>
                    </div>
                    <div class="col-md-6">
                        <fieldset class="form-group">
                            <label class="form-label semibold">Quantity</label>
                            <input type="text" class="form-control" name="quantity" value="{{$data->quantity}}" required>
                        </fieldset>
                    </div>

                    <div class="col-md-6">
                        <fieldset class="form-group">
                            <label class="form-label semibold">Net Quantity</label>
                            <input type="text" class="form-control" value="{{$data->remaining}}" disabled>
                        </fieldset>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <fieldset class="form-group">
                            <label class="form-label semibold">Manufacturer</label>
                            <select name="manufacturer_id" class="form-control">
                                <option value="">-</option>
                                @foreach($manufacturers as $manufacturer)
                                    <option value="{{$manufacturer->id}}" {{$data->manufacturer_id == $manufacturer->id ? 'selected' : ''}}>{{$manufacturer->name}}</option>
                                @endforeach
                            </select>
                        </fieldset>
                    </div>
                    <div class="col-md-6">
                        <fieldset class="form-group">
                            <label class="form-label semibold">Product Image</label>
                            <input type="file" class="form-control" name="image" value="0" accept="image/*">
                        </fieldset>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <fieldset class="form-group">
                            <label class="form-label semibold">File Name</label>
                            <input type="text" class="form-control" name="file_name">
                        </fieldset>
                    </div>
                    <div class="col-md-6">
                        <fieldset class="form-group">
                            <label class="form-label semibold">File</label>
                            <input type="file" class="form-control" name="file">
                        </fieldset>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <label class="form-label semibold">Tags</label>
                        <select class="select2" name="tags[]" multiple="multiple">
                            @foreach($tags as $tag)
                            <option value="{{$tag->id}}" {{ in_array($tag->id,$productTags) ? 'selected' : ''}}>{{$tag->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <label class="form-label semibold">Note</label>
                        <textarea class="form-control" name="note">{{$data->note}}</textarea>
                    </div>
                </div>


                <div class="row mt-2">
                    <div class="col-md-12">
                        <div class="col-md-6 float-right">
                            <button type="submit" class="btn btn-success float-right"><i class="fa fa-save"></i> Save</button>
                        </div>
                    </div>
                </div>
            </form>

        </div>

        <hr>
        <div class="row">
            <table id="table-sm" class="table table-bordered table-hover table-sm">
                <thead>
                <tr>
                    <th class="text-center">Created Date</th>
                    <th class="text-center">Name</th>
                    <th class="text-center">Filename</th>
                    <th class="text-center">Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($data->files as $value)
                <tr>
                    <td class="text-center">{{date('d.m.Y',strtotime($value->created_at))}}</td>
                    <td class="text-center">{{$value->name}}</td>
                    <td class="text-center">{{$value->path}}</td>
                    <td class="text-center">
                        <div class="">
                            <a target="_blank" href="{{$value->path}}"><button type="button" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i> Show PDF</button></a>
                            <form action="{{route('product.file-delete',$value->id)}}" method="POST">@csrf @method('delete')<button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Are you sure?')"><i class="fa fa-trash-o"></i> Delete</button></form>
                        </div>
                    </td>
                </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <hr>
        <div class="row">
            <h3>Received Product</h3>
            <table id="table-sm" class="table table-bordered table-hover table-sm">
                <thead>
                <tr>
                    <th class="text-center">Created At</th>
                    <th class="text-center">Created Name</th>
                    <th class="text-center">Use Area Name</th>
                    <th class="text-center">Process</th>
                    <th class="text-center">Quantity</th>
                    <th class="text-center">Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($data->pivot as $value)
                    @if(!$value->action)
                    <tr>
                        <td class="text-center">{{date('d.m.Y',strtotime($value->created_at))}}</td>
                        <td class="text-center">{{$value->product->name}}</td>
                        <td class="text-center">{{$value->useArea->name}}</td>
                        <td class="text-center">{{$value->action ? 'Supplied product' : 'Received product'}}</td>
                        <td class="text-center">{{$value->quantity}}</td>
                        <td class="text-center">
                            <div class="">
                                <a href="{{route('use-product.edit',$value->id)}}"><button type="button" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i> Show</button></a>
                            </div>
                        </td>
                    </tr>
                    @endif
                @endforeach
                </tbody>
            </table>
        </div>

        <hr>
        <div class="row">
        <h3>Supplied Product</h3>
            <table id="table-sm" class="table table-bordered table-hover table-sm">
                <thead>
                <tr>
                    <th class="text-center">Created At</th>
                    <th class="text-center">Created Name</th>
                    <th class="text-center">Use Area Name</th>
                    <th class="text-center">Process</th>
                    <th class="text-center">Quantity</th>
                    <th class="text-center">Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($data->pivot as $value)
                    @if($value->action)

                        <tr>
                        <td class="text-center">{{date('d.m.Y',strtotime($value->created_at))}}</td>
                        <td class="text-center">{{$value->product->name}}</td>
                        <td class="text-center">{{$value->useArea->name}}</td>
                        <td class="text-center">{{$value->action ? 'Supplied product' : 'Received product'}}</td>
                        <td class="text-center">{{$value->quantity}}</td>
                        <td class="text-center">
                            <div class="">
                                <a href="{{route('use-product.edit',$value->id)}}"><button type="button" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i> Show</button></a>
                            </div>
                        </td>
                    </tr>
                        @endif
                @endforeach
                </tbody>
            </table>
        </div>

    </div>
@endsection
