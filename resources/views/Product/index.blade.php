@extends('layout')
@section('content')
    <div class="container-fluid">
        <header class="section-header">
            <div class="tbl">
                <div class="tbl-row">
                    <div class="tbl-cell">
                        <h3>Productssssssss</h3>
                    </div>
                    <div class="float-right">
                        <a href="{{route('product.create')}}"> <button type="button" class="btn btn-warning"><i class="fa fa-plus"></i> Product Add</button></a>
                    </div>
                </div>
            </div>
        </header>
        <div class="box-typical box-typical-padding">
            <form action="{{route('product.index')}}" method="GET">
            <div class="row">
                <div class="col-md-2">
                    <fieldset class="form-group">
                        <label class="form-label semibold">Use Area</label>
                        <select name="use_area_id" class="form-control">
                            <option value="">All</option>
                            @foreach($useAreas as $useArea)
                                <option value="{{$useArea->id}}" {{\Illuminate\Support\Facades\Request::get('use_area_id') ? 'selected' : '' }}>{{$useArea->name}}</option>
                            @endforeach
                        </select>
                    </fieldset>
                </div>

                <div class="col-md-2">
                    <fieldset class="form-group">
                        <label class="form-label semibold">Product Name</label>
                       <input type="text" class="form-control" name="name" value="{{\Illuminate\Support\Facades\Request::get('name') }}">
                    </fieldset>
                </div>

                <div class="col-md-2">
                    <fieldset class="form-group">
                        <label class="form-label semibold">Manufacturer</label>
                        <select name="manufacturer_id" class="form-control">
                            <option value="">All</option>
                            @foreach($manufacturers as $manufacturer)
                                <option value="{{$manufacturer->id}}" {{\Illuminate\Support\Facades\Request::get('manufacturer_id') ? 'selected' : '' }}>{{$manufacturer->name}}</option>
                            @endforeach
                        </select>
                    </fieldset>
                </div>

                <div class="col-md-2">
                    <fieldset class="form-group">
                        <label class="form-label semibold">Tag</label>
                        <select name="tag_ids[]" class="select2" multiple>
                            @foreach($tags as $tag)
                                <option value="{{$tag->id}}" {{ !is_null(\Illuminate\Support\Facades\Request::get('tag_ids')) ? (in_array($tag->id,\Illuminate\Support\Facades\Request::get('tag_ids')) ? 'selected' : '') : ''}}>{{$tag->name}}</option>
                            @endforeach
                        </select>
                    </fieldset>
                </div>

                <div class="col-md-2 align-self-center">
                    <button class="btn btn-primary" type="submit"><i class="fa fa-search"></i></button>
                </div>
            </div>
            </form>

            <table id="table-sm" class="table table-bordered table-hover table-sm">
                <thead>
                <tr>
                    <th class="text-center" width="1">#</th>
                    <th class="text-center">Image</th>
                    <th class="text-center">Manufacturer</th>
                    <th class="text-center">Name</th>
                    <th class="text-center">Part Code</th>
                    <th class="text-center">Quantity</th>
                    <th class="text-center">Web Site</th>
                    <th class="text-center">Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($products as $value)
                    <tr>
                        <td class="text-center">{{$value->id}}</td>
                        <td class="text-center">@if($value->image)<img src="{{'/product_images/'.$value->image}}" width="150">@else - @endif </td>
                        <td class="text-center"> {{!empty($value->manufacturer_id) ? $value->manufacturer->name : '-'}}</td>
                        <td class="text-center">{{$value->name}}</td>
                        <td class="text-center">{{$value->part_code}}</td>
                        <td class="text-center">{{$value->quantity}}</td>
                        <td class="text-center">{{$value->web_site}}</td>
                        <td class="text-center" width="250">
                            <div class="row">
                                <div class="col-md-6">
                                    <a href="{{route('product.edit',$value->id)}}"><button type="button" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i> Edit</button></a>
                                </div>
                                @if(\Illuminate\Support\Facades\Auth::user()->delete_permission)
                                <div class="col-md-6">
                                    <form action="{{route('product.destroy',$value->id)}}" method="POST">@csrf @method('delete')<button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Are you sure?')"><i class="fa fa-trash-o"></i> Delete</button></form>
                                </div>
                                    @endif
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>


                <div class="d-flex justify-content-center">
                    {{ $products->appends(request()->input())->links() }}

                </div>

        </div>


    </div>
@endsection
